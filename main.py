#!/usr/bin/env python

__version__ = '1.0'
__author__ = 'Piotr Szulc'
__email__ = 'piotrszulc1011@gmail.com'
__copyright__ = "Copyright (c) 2020 Piotr Szulc"


"""Zadanie ze slajdu 22"""

height = float(1.77)
print("I am", height, "cm tall")
name = str("Piotr")
surname = str("Szulc")
print("My name is", name, surname)
age = int(21)
print("I am ", age, "years old")

"""Zadanie ze slajdu 29"""

class Mammals: pass
tiger = Mammals()

class Perfumes: pass
armani = Perfumes()

class Cakes: pass
cheesecake = Cakes()

print(tiger, armani, cheesecake)